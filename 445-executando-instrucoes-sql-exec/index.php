<?php

// Criando uma instância do PDO
/* 
1º parametro: DSN - 
    1º - mysql: é o driver da lib
    2º - host: é o caminho do endereco de acesso
    3º - dbname: o banco de dados usado
*/
/* 2º Parametro - user:  Usuário do banco, no caso em localhost é o root */
/* 3º Parametro - password:  Senha do usuário para acessar o banco, no caso do ambiente localhost, geralmente deixa a senha em branco */

$dsn = 'mysql:host=localhost;dbname=curso_php_pdo';
$user = 'root';
$pass = '';

// try: tenta algo
// Catch: pega o erro

try{
    $conexao = new PDO($dsn, $user, $pass);

    // Crio a query que quero executar
    // Essa query é uma DDL - Código de estrutura
    // IF NOT EXISTS: Verifica se a tabela já existe, caso não exista ele cria, caso exista ele não faz nada.
    $query = 'create table if not exists tb_usuarios(
        id int not null primary key auto_increment,
        nome varchar(50) not null,
        email varchar(100) not null,
        senha varchar(32) not null
    )';

    // Com o obj $conexao criado a partir da instância da classe PDO será executado o método exec() passando a query como parametro
    // O método exec() retorna apenas o numero de linhas modificadas ou removidas
    // Como a query executada é uma DDL, o retorno pelo exec será 0
    // Caso fosse um UPDATE, DELET ou INSERT iria retornar a quantidade de linhas afetadas
    // $retorno = $conexao->exec($query);

    // echo $retorno;

    // ==========================================================

    // Query de inserção, onde será usado o exec() para popular os valores
    // $query = "insert into tb_usuarios(nome, email, senha)values('Isa Basto', 'isa@teste.com.br', '123456')";

    // Query que deleta todos os usuários da tabela tb_usuarios
    $query = "delete from tb_usuarios";

    $return = $conexao->exec($query);
    echo $return;

}catch(PDOException $e){    
    echo "
        <div style='font-family: Calibri, sans-serif; padding: 6px; background-color: #d12121; border: 1px solid #800000; color: #fff; font-size: 14px; border-radius: 2px'>
            <strong>Mensagem de erro: </strong> {$e->getMessage()} | 
            <strong>Arquivo: </strong> {$e->getLine()} | 
            <strong>Linha: </strong> {$e->getLine()}
        </div>
    ";
}

