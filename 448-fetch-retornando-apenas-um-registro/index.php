<?php

// Criando uma instância do PDO
/* 
1º parametro: DSN - 
    1º - mysql: é o driver da lib
    2º - host: é o caminho do endereco de acesso
    3º - dbname: o banco de dados usado
*/
/* 2º Parametro - user:  Usuário do banco, no caso em localhost é o root */
/* 3º Parametro - password:  Senha do usuário para acessar o banco, no caso do ambiente localhost, geralmente deixa a senha em branco */

$dsn = 'mysql:host=localhost;dbname=curso_php_pdo';
$user = 'root';
$pass = '';

// try: tenta algo
// Catch: pega o erro

try {
    $conexao = new PDO($dsn, $user, $pass);

    /*
    // Crio a query de execução
    $query = "insert into tb_usuarios(nome, email, senha) values('Francisco Assis', 'francisco.sts@hotmail.com', '123456')";

    // Executo a query acima
    $conexao->exec($query);
    */

    // $query = "select * from tb_usuarios where id = 4";
    $query = "select * from tb_usuarios order by nome desc limit 1";
    $stmt = $conexao->query($query);

    // O método fetch() retorna apenas um registro.
    $lista = $stmt->fetch(PDO::FETCH_OBJ);

    var_dump($lista);
    
    // Para arrays de objetos
    echo "Nome: " . $lista->nome . "<br>";
    echo "Email: " . $lista->email . "<br>";
    echo "Senha: " . $lista->senha . "<br>";

} catch (PDOException $e) {
    echo "
        <div style='font-family: Calibri, sans-serif; padding: 6px; background-color: #d12121; border: 1px solid #800000; color: #fff; font-size: 14px; border-radius: 2px'>
            <strong>Mensagem de erro: </strong> {$e->getMessage()} | 
            <strong>Arquivo: </strong> {$e->getLine()} | 
            <strong>Linha: </strong> {$e->getLine()}
        </div>
    ";
}
