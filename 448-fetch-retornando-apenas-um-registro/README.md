# **Fetch - Retornando apenas um registro**

### O uso do **fetch()** serve?<br>
O **fetch()** retorna apenas um registro via PDO.<br>
Os seguintes parametros abaixo é passado no método.<br>

**PARAMETROS:**
- **PDO::FETCH_ASSOC**: Retorna um array associativo
- **PDO::FETCH_NUM**: Retorna array numérico.
- **PDO::FETCH_BOTH**: Retorna um array associativo e numéricos
- **PDO::FETCH_OBJ**: Retorna um array de objetos

<br><br>

## **IMPORTANTE**<br>
- No arquivo index.php contém trechos documentados com bastante detalhes.