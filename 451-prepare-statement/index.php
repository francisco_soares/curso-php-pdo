<?php


if (!empty($_POST['usuario']) && !empty($_POST['senha'])) {
    // Criando uma instâ ncia do PDO
    /* 
    1º parametro: DSN - 
        1º - mysql: é o driver da lib
        2º - host: é o caminho do endereco de acesso
        3º - dbname: o banco de dados usado
    */
    /* 2º Parametro - user:  Usuário do banco, no caso em localhost é o root */
    /* 3º Parametro - password:  Senha do usuário para acessar o banco, no caso do ambiente localhost, geralmente deixa a senha em branco */

    $dsn = 'mysql:host=localhost;dbname=curso_php_pdo';
    $user = 'root';
    $pass = '';

    // try: tenta algo
    // Catch: pega o erro

    try {
        $conexao = new PDO($dsn, $user, $pass);

        // Query insert
        // $query_insert = "insert into tb_usuarios(nome, email, senha) values('Francisco Soares', 'francisco.sts@hotmail.com', '123456')";
        // $conexao->query($query_insert);

        // $query_delete = "delete from tb_usuarios where id = '13'";
        // $conexao->query($query_delete);
        
        // Query select
        $query = "select * from tb_usuarios where ";
        $query .= " email = :usuario ";
        $query .= " AND senha = :senha ";

        // O prepare() retorna um PDO statement, assim conseguimos evitar o SQL Injection
        // A diferençã é que no prepare não executa a instrução, você tem que dar o comando para isso acontecer.
        $stmt = $conexao->prepare($query);

        // Para executar o método prepare(), você tem que criar os bindValues.
        // O bindValue() recebe dois parametros:
        // bindValue(<variavel-de-ligacao>, <o-valor-de-ligacao>)
        // :usuario e :senha é usado para ligação com a query que foi definida acima.
        $stmt->bindValue(':usuario', $_POST['usuario']);
        $stmt->bindValue(':senha', $_POST['senha']);

        // Executo a query junto com os bindValues criados
        $stmt->execute();

        // Retorno o resultado como um único array.
        $usuario = $stmt->fetch();

        var_dump($usuario);

        

    } catch (PDOException $e) {
        echo "
        <div style='font-family: Calibri, sans-serif; padding: 6px; background-color: #d12121; border: 1px solid #800000; color: #fff; font-size: 14px; border-radius: 2px'>
            <strong>Mensagem de erro: </strong> {$e->getMessage()} | 
            <strong>Arquivo: </strong> {$e->getLine()} | 
            <strong>Linha: </strong> {$e->getLine()}
        </div>
    ";
    }
}

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <form method="post" action="index.php">
        <input type="text" name="usuario" placeholder="Usuário">
        <br>
        <input type="password" name="senha" placeholder="Senha">
        <br>
        <button type="submit">Entrar</button>
    </form>

</body>

</html>