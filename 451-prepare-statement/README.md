# **SQL Injection**

![SQL Injection](img/img-1.png)

Ainda falando sobre o **SQL Injection**.<br>
Para contornar essa vulnerabilidade, usamos o **prepare**<br><br>

### **PREPARE**
Usando o **prepare** você criar uma instrução preparada, isso significa que os parametros recebeido passará por ele, onde será tratado antes de enviar para a query e executar o PDOStatement.

<br><br>

## **IMPORTANTE**<br>
- No arquivo index.php contém trechos documentados com bastante detalhes.