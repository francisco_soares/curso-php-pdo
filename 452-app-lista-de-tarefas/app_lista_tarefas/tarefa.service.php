<?php

class TarefaService
{

    private $conexao;
    private $tarefa;

    /**
     * Método construtor da classe que recebe dois parametro, obj Conexao e obj Tarefa
     * 
     * Com o OBJ Conexão sendo passado por parametro, temos um link de conexão com o banco de dados.
     * Dessa forma, consigo usar todos os recuros da classe Conexao().
     * 
     * 
     * Com o OBJ Tarefa sendo passado por paramtro, consigo acessar todos os arquivo e métodos da classe Tarefa().
     * 
     * Ambos os dois paremetros passados foram TIPADOS
     *
     * @param Conexao $conexao
     * @param Tarefa $tarefa
     */
    public function __construct(Conexao $conexao, Tarefa $tarefa)
    {
        $this->conexao = $conexao->conectar();
        $this->tarefa = $tarefa;
    }
    /**
     * Create: Método criado para criar/inserir registro no banco dedados
     *
     * @return void
     */
    public function inserir()
    {
        // Crio a query
        $query = "insert into tb_tarefas(tarefa) values(:tarefa)";
        // Preparo a query
        $stmt = $this->conexao->prepare($query);
        // Crio os binds
        $stmt->bindValue(':tarefa', $this->tarefa->__get('tarefa'));
        // Executo a query
        $stmt->execute();
    }

    /**
     * Read: Método criado para ler os dados do banco
     *
     * @return void
     */
    public function recuperar()
    {
        // Crio a query
        $query = '  
            select 
                t.id, s.status, t.tarefa 
            from 
                tb_tarefas as t
            left join
                tb_status as s on (t.id_status = s.id)';
        // Preparo a query
        $stmt = $this->conexao->prepare($query);
        // Executo a query
        $stmt->execute();
        // Retorno um array de OBJ.
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Update: Método criado para atualizar os dados do banco
     *
     * @return void
     */
    public function atualizar()
    {
        // Caso não queira usar o marcador ?, posso usar o marcador nomeado, ex: tarefa = :tarefa where id = :id
        $query = "update tb_tarefas set tarefa = ? where id = ?";
        $stmt = $this->conexao->prepare($query);
        $stmt->bindValue(1, $this->tarefa->__get('tarefa'));
        $stmt->bindValue(2, $this->tarefa->__get('id'));
        return $stmt->execute();
    }

    /**
     * Delete: Método criado para remover dados do banco.
     *
     * @return void
     */
    public function remover()
    {
        $query = 'delete from tb_tarefas where id = :id';
        $stmt = $this->conexao->prepare($query);
        $stmt->bindValue(':id', $this->tarefa->__get('id'));
        $stmt->execute();
    }

    /**
     * Método que atualiza o statu da tarefa.
     *
     * @return void
     */
    public function marcarRealizada()
    {
        $query = "update tb_tarefas set id_status = ? where id = ?";
        $stmt = $this->conexao->prepare($query);
        $stmt->bindValue(1, $this->tarefa->__get('id_status'));
        $stmt->bindValue(2, $this->tarefa->__get('id'));

        $stmt->execute();
    }

    public function recuperarTarefasPendentes()
    {
        // Crio a query
        $query = '  
            select 
                t.id, s.status, t.tarefa 
            from 
                tb_tarefas as t
            left join
                tb_status as s on (t.id_status = s.id)
            where
            t.id_status = :id_status';
        // Preparo a query
        $stmt = $this->conexao->prepare($query);
        $stmt->bindValue(":id_status", $this->tarefa->__get("id_status"));
        // Executo a query
        $stmt->execute();
        // Retorno um array de OBJ.
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }
}
