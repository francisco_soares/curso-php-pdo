<?php

class Tarefa
{
    // Atributos que são iguais a tabela do banco de dados
    private $id;
    private $id_status;
    private $tarefa;
    private $data_cadastro;

    /**
     * Método mágico __get(), usado para pegar o valor do atributo e retornar esse valor
     * 
     * Esse método foi criado para não ter que criar um novo método getNomeAtributo para cada atributo criado nessa classe.
     *
     * @param string $atributo
     * @return valor
     */
    public function __get($atributo)
    {
        return $this->$atributo;
    }

    /**
     * Método mágico __set(), usado para manipular os atributos privados criados nessa classe. Nele é passado dois atributos: 1º O atributo da classe, 2º - Seu valor
     * 
     * * Esse método foi criado para não ter que criar um novo método setNomeAtributo para cada atributo criado nessa classe.
     *
     * @param string $atributo
     * @param string $valor
     */
    public function __set($atributo, $valor)
    {
        $this->$atributo = $valor;
        return $this;
    }
}
