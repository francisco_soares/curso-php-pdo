<?php

class Conexao
{
    /**
     * Atributos da classe de Conexao()
     */
    private $host = 'localhost';
    private $dbname = 'curso_php_pdo';
    private $user = 'root';
    private $pass = '';

    /**
     * Método criado para fazer a conexão com o banco de dados
     *
     * @return PDO Connection
     */
    public function conectar()
    {
        /**
         * Uso do Try para caso tenha algum erro, trato no Catch, dessa forma posso até fazer uma lógica para gerar logs.
         */
        try {
            /**
             * Crio uma intância do OBJ PDO, que é nativo do PHP
             */
            $conexao = new PDO(
                "mysql:host={$this->host};dbname={$this->dbname}",
                "{$this->user}",
                "{$this->pass}"
            );

            // retorna a conexão assim que o métoro conectar() for chamado.
            return $conexao;
        } catch (PDOException $e) {
            echo "
                <div style='font-family: Calibri, sans-serif; padding: 6px; background-color: #d12121; border: 1px solid #800000; color: #fff; font-size: 14px; border-radius: 2px'>
                    <strong>Mensagem de erro: </strong> {$e->getMessage()} | 
                    <strong>Arquivo: </strong> {$e->getLine()} | 
                    <strong>Linha: </strong> {$e->getLine()}
                </div>
            ";
        }
    }
}
