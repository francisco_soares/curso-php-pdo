<?php

require "../app_lista_tarefas/tarefa.model.php";
require "../app_lista_tarefas/tarefa.service.php";
require "../app_lista_tarefas/conexao.php";

// Defino a variável $ação para receber a requisição via GET.
// Caso não retorne nada via GET, é definido o valor da própria vvariável que vem de outro local, como o arquivo todas_tarefas.php
$acao = isset($_GET['acao']) ? $_GET['acao'] : $acao;

echo $acao;

if ($acao  == 'inserir') {
    // Crio uma instância da classe Tarefa()
    $tarefa = new Tarefa();

    $tarefa->__set('tarefa', $_POST['tarefa']);

    // Crio uma instância da classe de Conexão
    $conexao = new Conexao();

    // Crio uma instância da classe TarefasService
    $tarefa_service = new TarefaService($conexao, $tarefa);
    $tarefa_service->inserir();

    // Direciono o usuário para a pagina nova_tarefa.php
    header('Location: nova_tarefa.php?inclusao=1');
} else if ($acao == 'recuperar') {

    // Crio uma instância da classe Tarefa()
    $tarefa = new Tarefa();
    // Crio uma instância da classe de Conexão
    $conexao = new Conexao();

    // Crio uma instância da classe TarefaService
    $tarefa_service = new TarefaService($conexao, $tarefa);
    // Pego o método recuperar()
    $tarefas = $tarefa_service->recuperar();
} else if ($acao == "atualizar") {

    // Instancio a Classe Tarefa()
    $tarefa = new Tarefa();

    // Seto os dados enviado via Post quando for atualizar uma tarefa.
    // Com o return $this; dentro do arquivo tarefa.model.php, eu posso usar o __set da forma abaixo.
    $tarefa->__set('id', $_POST['id'])
        ->__set('tarefa', $_POST['tarefa']);

    // Instancio a Conexao()
    $conexao = new Conexao();

    // Instancio a Classe TarefaService($objConexao, $objTarefa), onde passo os objs na classe
    // Aqui executo toda a regra de negócio onde fará atualização das tarefas.
    $tarefaService = new TarefaService($conexao, $tarefa);
    if ($tarefaService->atualizar()) {

        if (isset($_GET['pag']) && $_GET['pag'] == 'index') {
            header('location: index.php');
        } else {
            header('location: todas_tarefas.php');
        }
    }
} else if ($acao == "remover") {

    // Instancio a classe Tarefa()
    $tarefa = new Tarefa();

    // Seta o ID
    $tarefa->__set('id', $_GET['id']);

    // Instancio a classe conexao
    $conexao = new Conexao();

    // Instancio a classe TarefaService e passo os parametros OBJ
    $tarefaService = new TarefaService($conexao, $tarefa);

    // Chamo o método remover.
    $tarefaService->remover();

    // Após removido, volta para a tela todas_taredas.php
    if (isset($_GET['pag']) && $_GET['pag'] == 'index') {
        header('location: index.php');
    } else {
        header('location: todas_tarefas.php');
    }
} else if ($acao == "marcarRealizada") {

    // Instancio a classe Tarefa()
    $tarefa = new Tarefa();

    // Seta o ID da tarefa e o status.
    $tarefa->__set('id', $_GET['id'])
        ->__set("id_status", 2);

    // Instancio a classe conexao
    $conexao = new Conexao();

    // Instancio a classe TarefaService e passo os parametros OBJs(conexao, tarefa)
    $tarefaService = new TarefaService($conexao, $tarefa);

    // Chamo o método para atualizar a tarefa (marcarRealizada())
    $tarefaService->marcarRealizada();

    // Após removido, volta para a tela todas_taredas.php
    if (isset($_GET['pag']) && $_GET['pag'] == 'index') {
        header('location: index.php');
    } else {
        header('location: todas_tarefas.php');
    }
} else if ($acao == "recuperarTarefasPendentes") {

    // Crio uma instância da classe Tarefa()
    $tarefa = new Tarefa();
    $tarefa->__set("id_status", 1);


    // Crio uma instância da classe de Conexão
    $conexao = new Conexao();

    // Crio uma instância da classe TarefaService
    $tarefa_service = new TarefaService($conexao, $tarefa);
    // Pego o método recuperar()
    $tarefas = $tarefa_service->recuperarTarefasPendentes();
}
