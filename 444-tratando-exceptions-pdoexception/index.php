<?php

// Criando uma instância do PDO
/* 
1º parametro: DSN - 
    1º - mysql: é o driver da lib
    2º - host: é o caminho do endereco de acesso
    3º - dbname: o banco de dados usado
*/
/* 2º Parametro - user:  Usuário do banco, no caso em localhost é o root */
/* 3º Parametro - password:  Senha do usuário para acessar o banco, no caso do ambiente localhost, geralmente deixa a senha em branco */

$dsn = 'mysql:host=localhost;dbname=xcurso_php_pdos';
$user = 'root';
$pass = '';

// try: tenta algo
// Catch: pega o erro

try{
    $conexao = new PDO($dsn, $user, $pass);
}catch(PDOException $e){
    
    echo "
        <div style='font-family: Calibri, sans-serif; padding: 6px; background-color: #d12121; border: 1px solid #800000; color: #fff; font-size: 14px; border-radius: 2px'>
            <strong>Mensagem de erro: </strong> {$e->getMessage()} | 
            <strong>Arquivo: </strong> {$e->getFile()} | 
            <strong>Linha: </strong> {$e->getLine()}
        </div>
    ";

    /**
     * DICA IMPORTANTE
     * Caso caia no CATCH, você pode criar uma lógica para:
     * - Criar um log e adicionar no banco
     * - Redireionar o usuário para outra página onde informa que o sistema apresenta algum erro
     */

    /**
     * get_class_methods() - Mostra todos os métodos de um obj instanciado.
     */
    // var_dump(get_class_methods($e));
}

