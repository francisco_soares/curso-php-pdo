# **TRATANDO EXCEPTIONS (PDOException)**

![CRIANDO UMA CONEXÃO ENTRE O PHP E O MYSQL COM PDO](img/img-1.png)<br><br>

A abordagem do TRY foi feito em outro curso.<br><br>

**DICA IMPORTANTE**<br>
Caso caia no CATCH, você pode criar uma lógica para:
* Criar um log e adicionar no banco
* Redireionar o usuário para outra página onde informa que o sistema apresenta algum erro

