<?php


if (!empty($_POST['usuario']) && !empty($_POST['senha'])) {
    // Criando uma instâ ncia do PDO
    /* 
    1º parametro: DSN - 
        1º - mysql: é o driver da lib
        2º - host: é o caminho do endereco de acesso
        3º - dbname: o banco de dados usado
    */
    /* 2º Parametro - user:  Usuário do banco, no caso em localhost é o root */
    /* 3º Parametro - password:  Senha do usuário para acessar o banco, no caso do ambiente localhost, geralmente deixa a senha em branco */

    $dsn = 'mysql:host=localhost;dbname=curso_php_pdo';
    $user = 'root';
    $pass = '';

    // try: tenta algo
    // Catch: pega o erro

    try {
        $conexao = new PDO($dsn, $user, $pass);

        // Query
        $query =  "select * from tb_usuarios where ";
        $query .= " email = '{$_POST['usuario']}' ";
        $query .= " AND senha = '{$_POST['senha']}' ";

        echo $query;

        $stmt = $conexao->query($query);
        $usuario = $stmt->fetch();

        var_dump($usuario);

    } catch (PDOException $e) {
        echo "
        <div style='font-family: Calibri, sans-serif; padding: 6px; background-color: #d12121; border: 1px solid #800000; color: #fff; font-size: 14px; border-radius: 2px'>
            <strong>Mensagem de erro: </strong> {$e->getMessage()} | 
            <strong>Arquivo: </strong> {$e->getLine()} | 
            <strong>Linha: </strong> {$e->getLine()}
        </div>
    ";
    }
}

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <form method="post" action="index.php">
        <input type="text" name="usuario" placeholder="Usuário">
        <br>
        <input type="password" name="senha" placeholder="Senha">
        <br>
        <button type="submit">Entrar</button>
    </form>

</body>

</html>