# **SQL Injection**

![SQL Injection](img/img-1.png)

O **SQL Injection** é um metodo de inserir instruções **SQL** de alguma forma em uma aplicação... geralmente em um formulário é uma das formas de ataque.
<br><br>

### **FORMA DE SQL INJECTION NO FORMULÁRIO**
Para fazer o **SQL Injection** no formulário, você pode inserir a seguinte instrução no campo password.:<br>
```
gotham797'; delete from tb_usuarios where 'a' = 'a
```
Lembrando que para isso funcionar, a pessoa precisa saber a estrutura do seu banco... mas, não é melhor arriscar.

<br><br>

## **IMPORTANTE**<br>
- No arquivo index.php contém trechos documentados com bastante detalhes.