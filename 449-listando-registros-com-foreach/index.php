<?php

// Criando uma instância do PDO
/* 
1º parametro: DSN - 
    1º - mysql: é o driver da lib
    2º - host: é o caminho do endereco de acesso
    3º - dbname: o banco de dados usado
*/
/* 2º Parametro - user:  Usuário do banco, no caso em localhost é o root */
/* 3º Parametro - password:  Senha do usuário para acessar o banco, no caso do ambiente localhost, geralmente deixa a senha em branco */

$dsn = 'mysql:host=localhost;dbname=curso_php_pdo';
$user = 'root';
$pass = '';

// try: tenta algo
// Catch: pega o erro

try {
    $conexao = new PDO($dsn, $user, $pass);

    /*
    // Crio a query de execução
    $query = "insert into tb_usuarios(nome, email, senha) values('Francisco Assis', 'francisco.sts@hotmail.com', '123456')";

    // Executo a query acima
    $conexao->exec($query);
    */

    // $query = "select * from tb_usuarios where id = 4";
    $query = "select * from tb_usuarios";
    // $stmt = $conexao->query($query);

    // Também posso usar o foreach com o $conexao->query($query);

    // Técnica: QUERY ONE THE ONE
    foreach ($stmt = $conexao->query($query) as $usuario) :
        echo "<b>Nome:</b> " . $usuario['nome'] . "<br>";
        echo "<b>Email:</b> " . $usuario['email'] . "<br>";
        echo "<b>Senha:</b> " . $usuario['senha'] . "<br><br>";
    endforeach;

    // O método fetch() retorna apenas um registro.
    // $listas_usuarios = $stmt->fetchAll(PDO::FETCH_ASSOC);

    // var_dump($listas_usuarios);

    // Listando todos os usuários com o foreach()
    // foreach($listas_usuarios as $usuario){
    //     echo "<b>Nome:</b> " . $usuario['nome'] . "<br>";
    //     echo "<b>Email:</b> " . $usuario['email'] . "<br>";
    //     echo "<b>Senha:</b> " . $usuario['senha'] . "<br><br>";
    // }

} catch (PDOException $e) {
    echo "
        <div style='font-family: Calibri, sans-serif; padding: 6px; background-color: #d12121; border: 1px solid #800000; color: #fff; font-size: 14px; border-radius: 2px'>
            <strong>Mensagem de erro: </strong> {$e->getMessage()} | 
            <strong>Arquivo: </strong> {$e->getLine()} | 
            <strong>Linha: </strong> {$e->getLine()}
        </div>
    ";
}
