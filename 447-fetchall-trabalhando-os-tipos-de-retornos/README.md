# **FetchAll - trabalhando os tipos de retornos**

Como o título já diz, nesse exemplo é abordados os tipos de retorno que o método **fetchAll()** nos trás.<br><br>

**TIPO DE RETORNO:**
- **fetchAll()**: Dessa forma retorna um array associativo e numéricos
- **fetchAll(PDO::FETCH_ASSOC)**: Retorna um array associativo
- **fetchAll(PDO::FETCH_NUM)**: Retorna array numérico.
- **fetchAll(PDO::FETCH_BOTH)**: Retorna um array associativo e numéricos
- **fetchAll(PDO::FETCH_OBJ)**: Retorna um array de objetos

<br><br>

## **IMPORTANTE**<br>
- No arquivo index.php contém trechos documentados com bastante detalhes.