<?php
// Mostra os erros
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);


// Criando uma instância do PDO
/* 
1º parametro: DSN - 
    1º - mysql: é o driver da lib
    2º - host: é o caminho do endereco de acesso
    3º - dbname: o banco de dados usado
*/
/* 2º Parametro - user:  Usuário do banco, no caso em localhost é o root */
/* 3º Parametro - password:  Senha do usuário para acessar o banco, no caso do ambiente localhost, geralmente deixa a senha em branco */

$dsn = 'mysql:host=localhost;dbname=curso_php_pdo';
$user = 'root';
$pass = '';

// try: tenta algo
// Catch: pega o erro

try {
    $conexao = new PDO($dsn, $user, $pass);

    
    // Crio a query de execução
    // $query = "insert into tb_usuarios(nome, email, senha) values('Francisco Assis', 'francisco.sts@hotmail.com', '123456')";
    // $query = "insert into tb_usuarios(nome, email, senha) values('Isa Basto', 'isa@hotmail.com', '123456')";
    // $query = "insert into tb_usuarios(nome, email, senha) values('Saitama', 'saitama@hotmail.com', '123456')";
    

    // Executo a query acima
    // $conexao->exec($query);
    

    $query = "select * from tb_usuarios";
    $stmt = $conexao->query($query); // O método QUERY retorna o PDOStatement

    // Apenas com o fetchAll() ele retorna um array associativo e indices numeros
    $lista = $stmt->fetchAll();

    // echo "<pre>";
    // print_r($lista);
    // echo "</pre>";

    // echo "Nome: " . $lista[0]['nome'] . "<br>";
    // echo "Email: " . $lista[0]['email'] . "<br>";
    // echo "Senha: " . $lista[0]['senha'] . "<br>";   
    
    /**
     * Pego todos os registros que estão dentro da tabela usando o FOR passando o indece do array com a variável $i.
     */
    for($i = 0; $i < count($lista); $i++){
        echo "Nome: " . $lista[$i]['nome'] . "<br>";
        echo "E-mail: " . $lista[$i]['email'] . "<br>";
        echo "Senha: " . $lista[$i]['senha'] . "<br><hr>";
    }
    echo "<br>************************************<hr><br>";

     /**
     * Pego todos os registros que estão dentro da tabela usando o FOREACH
     */
    foreach($lista as $dados){
        echo "Nome: " . $dados['nome'] . "<br>";
        echo "E-mail: " . $dados['email'] . "<br>";
        echo "Senha: " . $dados['senha'] . "<br><hr>";
    }

} catch (PDOException $e) {
    echo "
        <div style='font-family: Calibri, sans-serif; padding: 6px; background-color: #d12121; border: 1px solid #800000; color: #fff; font-size: 14px; border-radius: 2px'>
            <strong>Mensagem de erro: </strong> {$e->getMessage()} | 
            <strong>Arquivo: </strong> {$e->getLine()} | 
            <strong>Linha: </strong> {$e->getLine()}
        </div>
    ";
}
