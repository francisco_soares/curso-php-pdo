# **PDOStatement Object(Query) com fetchAll**

![PDOStatement Object(Query) com fetchAll](img/img-1.png)<br><br>


Nessa aula aborda o uso do método **Query** do **PDO** para executar a ação ao banco de dados.<br>
Assim, o **PDO** retorna um **PDOStatement**.<br><br>

## **IMPORTANTE**<br>
- No arquivo index.php contém trechos documentados com bastante detalhes.