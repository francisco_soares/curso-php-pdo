# **CRIANDO UMA CONEXÃO ENTRE O PHP E O MYSQL COM PDO**

![CRIANDO UMA CONEXÃO ENTRE O PHP E O MYSQL COM PDO](img/img-1.png)

### **Criando uma instância do PDO**<br><br>

**1º parametro: DSN:**<br>

1. **mysql:** é o driver da lib
2. **host:** é o caminho do endereco de acesso
3. **dbname:** o banco de dados usado

**2º Parametro**<br>
1. **user:**  Usuário do banco, no caso em localhost é o root<br><br>

**3º Parametro**<br>
1. **password:**  Senha do usuário para acessar o banco, no caso do ambiente localhost, geralmente deixa a senha em branco */

