# **INTRODUÇÃO AO PDO**

![Introdução ao PDO](img/img-1.png)

### **O QUE É PDO?**<br>
O **PDO(PHP Data Objects)** consiste em um conjunto de objetos que nos auxiliam no trabalho com o banco de dados.

### **PARA QUE SERVE O PDO?**<br>
Ser para um padrão para conexão com o banco de dados.

### **Qual a vantagem de se trabalhar com o PDO?**<br>
- Segurança nas conexão com o banco
- Padrão na alteração de banco de dados mais eficaz<br><br>

Para trocar o banco de dados, apenas precisa habilitar o driver.<br>
Por padrão no PHP o driver que vem é o **pdo_mysql**, isso é alterado no **php.ini**.
